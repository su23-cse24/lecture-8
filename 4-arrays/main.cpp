#include <iostream>
using namespace std;

void printArray(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        cout << "arr: " << (long) (arr + i) << endl;
        cout << "arr-> " << *(arr + i) << endl;
    }
}

int main() {

    const int size = 5;
    int arr[size] = {1, 3, 5, 7, 9};
    printArray(arr, size);

    return 0;
}