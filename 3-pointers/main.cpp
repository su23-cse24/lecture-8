#include <iostream>
using namespace std;

int main() {

    int x = 7;
    int* px = &x;

    char c = 'A';
    char* pc = &c;

    cout << "x: " << x << endl;
    cout << "px: " << (long) px << endl;
    cout << "px-> " << *px << endl;
    cout << endl;
    cout << "c: " << c << endl;
    cout << "pc: " << (long) pc << endl;
    cout << "pc-> " << *pc << endl;

    // change the value of x and c
    *px = 5;
    *pc = 'B';

    cout << endl;
    cout << "x: " << x << endl;
    cout << "px: " << (long) px << endl;
    cout << "px-> " << *px << endl;
    cout << endl;
    cout << "c: " << c << endl;
    cout << "pc: " << (long) pc << endl;
    cout << "pc-> " << *pc << endl;

    return 0;
}