#include <iostream>
using namespace std;

int main() {

    int x = 7;
    int y = 42;
    double z = 3.14;
    bool b = false;

    cout << "x: " << x << endl;
    cout << "&x: " << (long) &x << endl;
    cout << endl;
    cout << "y: " << y << endl;
    cout << "&y: " << (long) &y << endl;
    cout << endl;
    cout << "z: " << z << endl;
    cout << "&z: " << (long) &z << endl;
    cout << endl;
    cout << "b: " << b << endl;
    cout << "&b: " << (long) &b << endl;
 
    return 0;
}