#ifndef SEARCH_H
#define SEARCH_H

#include <vector>

// [1, 2, 3, 4, 5]
// target = 5
// l = 3
// r = 4
// m = 3


bool binarySearch(std::vector<int> v, int target) {
    int l = 0;
    int r = v.size() - 1;

    while (l <= r) {
        int m = (l + r) / 2;

        if (v[m] == target) {
            return true;
        } if (v[m] < target) {
            l = m + 1;
        } else {
            r = m - 1;
        }
    }

    return false;
}

#endif