#include <iostream>
#include <vector>
#include "search.h"
using namespace std;

int main() {

    vector<int> v = {1, 2, 3, 4, 5};
    int target = 0;

    bool found = binarySearch(v, target);

    if (found) {
        cout << "FOUND IT" << endl;
    } else {
        cout << "NOT FOUND" << endl;
    }

 
    return 0;
}