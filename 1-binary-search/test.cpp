#include <igloo/igloo.h>
#include "search.h"

using namespace igloo;

Context(TestAll){
    Spec(MiddleElementFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 3), IsTrue());
    }

    Spec(FirstElementFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 1), IsTrue());
    }

    Spec(LastElementFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 5), IsTrue());
    }

    Spec(SmallerElementNotFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 0), IsFalse());
    }

    Spec(LargerElementNotFound){
        std::vector<int> numbers = {1,2,3,4,5};
        Assert::That(binarySearch(numbers, 42), IsFalse());
    }
};

int main(int argc, const char* argv[]){
    return TestRunner::RunAllTests(argc, argv);
}